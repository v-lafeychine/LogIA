use std::fmt::{Display, Formatter};

use super::{Clause, Identifier, Literal};
use crate::solver::horn::HornModel;
use crate::solver::Model;

#[derive(Clone, Debug)]
/// A [HornClause] is a disjunctive clause with at most one positive literal.
///
/// A Horn clause is of the form `!(p_1) \/ ... \/ !(p_n) \/ q`, where:
///  - `p_i` are variables
///  - `q` can be a variable, or `False`
///
/// Hence, we can rewrite the Horn clause as: `p_1 /\ ... /\ p_n => q`.
pub struct HornClause {
    /// The unique identifier.
    #[allow(dead_code)]
    id: Identifier<Self>,

    /// The hypothesis of the clause.
    #[allow(dead_code)]
    hypothesis: Vec<Literal>,

    /// The conclusion can be a variable or `False`.
    #[allow(dead_code)]
    conclusion: Option<Literal>,
}

impl HornClause {
    pub fn new(id: Identifier<Self>, hypothesis: Vec<Literal>, conclusion: Option<Literal>) -> Self {
        HornClause { id, hypothesis, conclusion }
    }

    pub fn hypothesis(&self) -> &Vec<Literal> {
        &self.hypothesis
    }

    pub fn conclusion(&self) -> &Option<Literal> {
        &self.conclusion
    }

    /// checks if all the hypothesis of the [HornClause] are
    /// verified in the [HornModel]
    pub fn can_deduce(&self, model: &HornModel) -> bool {
        self.hypothesis().iter().all(|lit| model.valuation(lit.id()).unwrap())
    }

    pub fn is_sat(&self, model: &HornModel) -> bool {
        if self.can_deduce(model) {
            return true;
        }

        let Some(conclusion) = self.conclusion() else { return false; };

        model.valuation(conclusion.id()).unwrap()
    }
}

impl Display for HornClause {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "horn(")?;
        if self.hypothesis().is_empty() {
            write!(f, "⊤")?;
        } else {
            let mut iter = self.hypothesis().iter();

            iter.next().map(|literal| write!(f, "{}", literal.id())).unwrap_or(Ok(()))?;
            iter.try_for_each(|literal| write!(f, "∧{}", literal.id()))?;
        }
        if let Some(conclusion) = self.conclusion() {
            write!(f, "⇒{}", conclusion.id())?
        } else {
            write!(f, "⇒⊥")?
        }
        write!(f, ")")
    }
}

impl Clause for HornClause {}
