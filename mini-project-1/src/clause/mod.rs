use std::fmt::{Debug, Display, Formatter};

use crate::identifier::Identifier;

pub mod horn;
pub mod xor;

#[derive(Clone, Copy, Debug, PartialOrd, Ord)]
/// A [Literal] is a boolean variable, defined by a unique [Identifier].
pub struct Literal {
    id: Identifier<Literal>,
    value: bool,
}

impl Display for Literal {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.id == Identifier::new(0) {
            return if self.value { write!(f, "⊤") } else { write!(f, "⊥") };
        }

        match self.value {
            true => write!(f, "{}", self.id),
            false => write!(f, "-{}", self.id),
        }
    }
}

impl PartialEq for Literal {
    fn eq(&self, rhs: &Self) -> bool {
        self.id == rhs.id
    }
}

impl Eq for Literal {}

impl Literal {
    /// Create a new [Literal] of value `value`.
    pub fn new(id: Identifier<Literal>, value: bool) -> Self {
        Self { id, value }
    }

    #[allow(dead_code)]
    pub fn r#false() -> Self {
        Self::new(Identifier::new(0), false)
    }

    pub fn r#true() -> Self {
        Self::new(Identifier::new(0), true)
    }

    /// Get the identifier of a [Literal].
    pub fn id(&self) -> Identifier<Self> {
        self.id
    }

    /// Check if the [Literal] set to `True`.
    pub fn is_true(&self) -> bool {
        self.value
    }

    /// Check if the [Literal] set to `False`.
    pub fn is_false(&self) -> bool {
        !(self.is_true())
    }

    /// Set the [Literal] to `value`.
    pub fn set(&mut self, value: bool) {
        self.value = value;
    }
}

/// A [Clause] is a set of [Literal]s, composed by a specific logic operator.
pub trait Clause {}
