use std::fmt::{Display, Formatter};

use log::{log_enabled, trace};

use super::{Clause, Identifier, Literal};

#[derive(Clone, Debug)]
/// A [XorClause] is a conjonction of `xor` operator on literals.
pub struct XorClause {
    /// The unique identifier.
    #[allow(dead_code)]
    id: Identifier<Self>,

    /// The literals of the clause.
    pub literals: Vec<Literal>,
}

impl XorClause {
    /// Creates a new [XorClause] from a list of literals.
    pub fn new(id: Identifier<Self>, literals: Vec<Literal>) -> Self {
        XorClause { id, literals }
    }

    /// Get a substitution for propagation phase.
    pub fn get_substitution(&self) -> Option<(Literal, Vec<Literal>)> {
        let mut clause_iter = self.literals.iter();

        let Some(key) = clause_iter.next() else {
            trace!("Unable to get a substitution from xor(): UNSAT");
            trace!("");

            return None;
        };

        let mut values: Vec<Literal> = clause_iter.cloned().collect();

        if !values.iter().any(|value| *value == Literal::r#true()) {
            values.push(Literal::r#true());
        } else {
            values.retain(|value| *value != Literal::r#true());
        }

        Some((*key, values))
    }

    /// Simplifies the [XorClause], using `xor` properties.
    pub fn simplify(&mut self) {
        trace!("Simplifying {}", self);

        // Remove negation literals.
        let mut append = false;

        self.literals.iter_mut().filter(|literal| literal.is_false()).for_each(|literal| {
            literal.set(true);
            append = !append;
        });

        if append {
            self.literals.push(Literal::new(Identifier::new(0), true));
        }

        // Remove duplicate literals.
        let mut literals: Vec<Literal> = Vec::new();

        self.literals.iter().for_each(|literal| {
            if !literals.contains(literal) {
                literals.push(*literal);
            } else {
                literals.retain(|l| l != literal);
            }
        });

        if let Some(x) = literals.get(0) {
            if x.id() == Identifier::new(0) {
                literals.sort_by(|a, b| b.cmp(a));
            }
        }
        self.literals = literals;

        trace!("> Got: {}", self);
        trace!("");
    }

    /// Apply a substitution to the [XorClause].
    pub fn substitute(&mut self, key: &Literal, values: &[Literal]) {
        if log_enabled!(log::Level::Trace) {
            let values_str = values.iter().map(|v| v.to_string()).collect::<Vec<_>>().join(", ");

            trace!("Substitute {} with ({}) in {}", key, values_str, self);
        }

        let indexes = self
            .literals
            .iter()
            .enumerate()
            .filter(|(_, literal)| literal.id() == key.id())
            .map(|(index, _)| index)
            .collect::<Vec<usize>>();

        indexes.iter().for_each(|index| {
            self.literals.splice(index ..= index, values.iter().cloned());
        });

        trace!("> Got: {}", self);
        trace!("");
    }
}

impl Display for XorClause {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "xor(")?;

        let mut iter = self.literals.iter();

        iter.next().map(|literal| write!(f, "{}", literal)).unwrap_or(Ok(()))?;
        iter.try_for_each(|literal| write!(f, ", {}", literal))?;

        write!(f, ")")
    }
}

impl Clause for XorClause {}
