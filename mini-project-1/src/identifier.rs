use std::marker::PhantomData;

use derive_more::{Deref, Display};

#[derive(Debug, Deref, Display, PartialOrd, Ord)]
#[display(fmt = "{}", _0)]
/// A [Identifier] to distinguish different elements of same type.
pub struct Identifier<T>(#[deref] usize, PhantomData<T>);

/// Implementation required as [Identifier] is implemented with a [PhantomData].
impl<T> Clone for Identifier<T> {
    fn clone(&self) -> Self {
        Identifier::new(self.0)
    }
}

impl<T> Copy for Identifier<T> {}

impl<T> PartialEq for Identifier<T> {
    fn eq(&self, rhs: &Self) -> bool {
        self.0 == rhs.0
    }
}

impl<T> Eq for Identifier<T> {}

impl<T> Identifier<T> {
    /// Create a new [Identifier] of identifiant `id`.
    pub fn new(id: usize) -> Self {
        Self(id, PhantomData)
    }
}
