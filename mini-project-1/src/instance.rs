use crate::clause::horn::HornClause;
use crate::clause::xor::XorClause;
use crate::clause::Clause;
use crate::parser::horn::HornParser;
use crate::parser::xor::XorParser;
use crate::parser::Parser;
use crate::solver::horn::{HornModel, HornSolver};
use crate::solver::xor::{XorModel, XorSolver};
use crate::solver::{Model, Solver};

/// A [Instance] represent a problem with the associated [Solver].
pub trait Instance {
    type Clause: Clause;
    type Model: Model;
    type Parser: Parser<Self::Clause>;
    type Solver: Solver<Self::Clause, Self::Model>;
}

pub enum Horn {}

impl Instance for Horn {
    type Clause = HornClause;
    type Model = HornModel;
    type Parser = HornParser;
    type Solver = HornSolver;
}

pub enum Xor {}

impl Instance for Xor {
    type Clause = XorClause;
    type Model = XorModel;
    type Parser = XorParser;
    type Solver = XorSolver;
}
