use std::fs::read_to_string;
use std::path::Path;

use anyhow::Result;

use crate::clause::Clause;

pub mod horn;
pub mod xor;

/// A [Parser] reads a problem from input, in DIMACS-like format.
pub trait Parser<C: Clause> {
    /// Parse a problem from `input` string.
    fn parse(input: &str) -> Result<Vec<C>>;

    /// Parse a problem from `path` file.
    fn parse_file(path: &Path) -> Result<Vec<C>> {
        Self::parse(&read_to_string(path)?)
    }
}
