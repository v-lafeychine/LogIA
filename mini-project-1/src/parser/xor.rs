use anyhow::{bail, Result};
use pest::iterators::Pairs;
use pest::Parser;

use crate::clause::xor::XorClause;
use crate::clause::Literal;
use crate::identifier::Identifier;

#[derive(Parser)]
#[grammar = "parser/xor.pest"]
struct PestParser;

pub struct XorParser {
    nb_clauses: usize,
    nb_literals: usize,
}

impl XorParser {
    fn new(nb_clauses: usize, nb_literals: usize) -> XorParser {
        XorParser { nb_clauses, nb_literals }
    }

    fn parse_clause(&mut self, pairs: Pairs<Rule>, index: usize) -> Result<XorClause> {
        if index >= self.nb_clauses {
            bail!("Too many clauses");
        }

        let mut literals = Vec::new();

        for pair in pairs {
            let identifier = pair.as_str().parse::<isize>()?;

            if identifier == 0 || identifier.abs() > self.nb_literals as isize {
                bail!("Literal {} is not allowed", identifier.abs());
            }

            literals.push(Literal::new(Identifier::new(identifier.abs().try_into()?), identifier > 0));
        }

        Ok(XorClause::new(Identifier::new(index), literals))
    }
}

fn parse_dimacs_header(pairs: &mut Pairs<Rule>) -> Result<[usize; 2]> {
    Ok(pairs
        .next()
        .unwrap_or_else(|| unreachable!())
        .into_inner()
        .map(|pair| pair.as_str().parse())
        .try_collect::<Vec<usize>>()?
        .as_slice()
        .try_into()
        .unwrap_or_else(|_| unreachable!()))
}

impl super::Parser<XorClause> for XorParser {
    fn parse(input: &str) -> Result<Vec<XorClause>> {
        let mut pairs = PestParser::parse(Rule::Problem, input)?;
        let [nb_literals, nb_clauses] = parse_dimacs_header(&mut pairs)?;
        let mut parser = XorParser::new(nb_clauses, nb_literals);

        pairs
            .enumerate()
            .map(|(index, pair)| parser.parse_clause(pair.into_inner(), index))
            .try_collect::<Vec<XorClause>>()
    }
}
