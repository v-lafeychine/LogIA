use std::cell::OnceCell;

use anyhow::{bail, Result};
use pest::iterators::Pairs;
use pest::Parser;

use crate::clause::horn::HornClause;
use crate::clause::Literal;
use crate::identifier::Identifier;

#[derive(Parser)]
#[grammar = "parser/horn.pest"]
struct PestParser;

pub struct HornParser {
    nb_clauses: usize,
    nb_literals: usize,
}

impl HornParser {
    fn new(nb_clauses: usize, nb_literals: usize) -> HornParser {
        HornParser { nb_clauses, nb_literals }
    }

    fn parse_clause(&mut self, pairs: Pairs<Rule>, index: usize) -> Result<HornClause> {
        if index >= self.nb_clauses {
            bail!("Too many clauses");
        }

        let mut literals = Vec::new();
        let conclusion: OnceCell<Literal> = OnceCell::new();

        for pair in pairs {
            let identifier = pair.as_str().parse::<isize>()?;

            if identifier == 0 || identifier.abs() > self.nb_literals as isize {
                bail!("Literal {} is not allowed", identifier.abs());
            }

            let literal = Literal::new(Identifier::new(identifier.abs().try_into()?), identifier > 0);

            if identifier > 0 {
                conclusion.set(literal).or_else(|_| bail!("Multiple positive literals"))?;
            } else {
                literals.push(literal);
            }
        }

        Ok(HornClause::new(Identifier::new(index), literals, conclusion.into_inner()))
    }
}

fn parse_dimacs_header(pairs: &mut Pairs<Rule>) -> Result<[usize; 2]> {
    Ok(pairs
        .next()
        .unwrap_or_else(|| unreachable!())
        .into_inner()
        .map(|pair| pair.as_str().parse())
        .try_collect::<Vec<usize>>()?
        .as_slice()
        .try_into()
        .unwrap_or_else(|_| unreachable!()))
}

impl super::Parser<HornClause> for HornParser {
    fn parse(input: &str) -> Result<Vec<HornClause>> {
        let mut pairs = PestParser::parse(Rule::Problem, input)?;
        let [nb_literals, nb_clauses] = parse_dimacs_header(&mut pairs)?;
        let mut parser = HornParser::new(nb_clauses, nb_literals);

        pairs
            .enumerate()
            .map(|(index, pair)| parser.parse_clause(pair.into_inner(), index))
            .try_collect::<Vec<HornClause>>()
    }
}
