use crate::clause::{Clause, Literal};
use crate::identifier::Identifier;

pub mod horn;
pub mod xor;

/// A [Solver] solves a problem for a specific set of [Clause]s.
pub trait Solver<C: Clause, M: Model> {
    /// Solve a SAT problem `problem`
    fn solve(problem: &[C]) -> Option<M>;
}

/// A [Model] is a mapping from variables to truth values.
pub trait Model {
    /// Returns the valuation of a specific [Literal].
    fn valuation(&self, id: Identifier<Literal>) -> Option<bool> {
        self.valuations().iter().find(|l| id == l.id()).map(|literal| literal.is_true())
    }

    /// Returns the valuations of the [Model].
    fn valuations(&self) -> &[Literal];
}
