use log::trace;

use super::{Model, Solver};
use crate::clause::horn::HornClause;
use crate::clause::Literal;
use crate::solver::Identifier;

/// A [HornSolver] is a specific [Solver] for [HornClause].
pub struct HornSolver {
    model: HornModel,
    problem: Vec<HornClause>,
}

impl Solver<HornClause, HornModel> for HornSolver {
    fn solve(problem: &[HornClause]) -> Option<HornModel> {
        HornSolver::new(problem).solve()
    }
}

impl HornSolver {
    fn new(problem: &[HornClause]) -> HornSolver {
        HornSolver { model: HornModel::new(), problem: problem.to_vec() }
    }

    /// Returns a [HornModel] or () depending on the satisfiability of the
    /// [HornSolver] iterates unit propagation until either:
    ///   - a contradiction is found
    ///   - the model satisfies the problem
    fn solve(mut self) -> Option<HornModel> {
        let mut changed = true;

        while changed {
            changed = false;

            for clause in &self.problem {
                if !(clause.can_deduce(&self.model)) {
                    continue;
                }

                let Some(new_lit) = clause.conclusion() else { return None;};

                if self.model.valuation(new_lit.id()).unwrap() {
                    continue;
                }
                trace!("added literal {} evaluated as true to the model", new_lit.id());
                trace!("this was deduced from clause {}", clause);
                self.model.add_val(new_lit);
                changed = true;
            }
        }

        Some(self.model)
    }
}

#[derive(Debug)]
pub struct HornModel {
    model: Vec<Literal>,
}

impl HornModel {
    fn new() -> HornModel {
        HornModel { model: Vec::new() }
    }

    /// Adds the evaluation of a literal to true in the [HornModel]
    fn add_val(&mut self, literal: &Literal) {
        if !self.model.contains(literal) {
            self.model.push(*literal);
        }
    }
}

impl Model for HornModel {
    fn valuations(&self) -> &[Literal] {
        &self.model
    }

    fn valuation(&self, id: Identifier<Literal>) -> Option<bool> {
        Some(self.model.iter().any(|lit| lit.id() == id))
    }
}
