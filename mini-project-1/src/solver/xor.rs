use derive_more::{Deref, DerefMut};
use log::{info, log_enabled, trace};

use super::{Model, Solver};
use crate::clause::xor::XorClause;
use crate::clause::Literal;
use crate::identifier::Identifier;

/// A [XorSolver] is a specific [Solver] for [XorClause].
#[derive(Debug)]
pub struct XorSolver {
    state: XorState,
}

impl Solver<XorClause, XorModel> for XorSolver {
    fn solve(problem: &[XorClause]) -> Option<XorModel> {
        XorSolver::new(problem).solve()
    }
}

impl XorSolver {
    fn new(problem: &[XorClause]) -> Self {
        XorSolver { state: XorState::new(problem) }
    }

    fn solve(&mut self) -> Option<XorModel> {
        let n = self.state.problem.len() + 1;

        while self.state.model.len() < n {
            info!("=== Propagation phase ({} / {}) ===", self.state.model.len(), n);
            info!("");

            if !self.state.propagate() {
                return None;
            }
        }

        info!("=== Resolve phase ===");
        info!("");

        Some(self.state.model.resolve())
    }
}

#[derive(Debug)]
struct XorState {
    problem: Vec<XorClause>,
    model: XorPartialModel,
}

impl XorState {
    fn new(problem: &[XorClause]) -> Self {
        XorState { problem: problem.to_vec(), model: XorPartialModel::new() }
    }

    fn propagate(&mut self) -> bool {
        self.problem.iter_mut().for_each(|clause| clause.simplify());

        let mut iter = self.problem.iter_mut();

        let Some((key, values)) = iter.next().unwrap().get_substitution() else { return false; };

        if log_enabled!(log::Level::Trace) {
            let values_str = match values[..] {
                [] => "⊥".to_string(),
                [x] => x.to_string(),
                _ => format!("({})", values.iter().map(|v| v.to_string()).collect::<Vec<_>>().join(", ")),
            };

            trace!("Associate {} to {} in the model", key, values_str);
            trace!("");
        }

        iter.for_each(|clause| clause.substitute(&key, &values));

        self.model.push((key.id(), XorClause::new(Identifier::new(*key.id()), values)));

        self.problem.remove(0);

        true
    }
}

#[derive(Debug, Deref, DerefMut)]
struct XorPartialModel(Vec<(Identifier<Literal>, XorClause)>);

impl XorPartialModel {
    fn new() -> Self {
        Self(vec![(Identifier::new(0), XorClause::new(Identifier::new(0), vec![Literal::r#true()]))])
    }

    fn resolve(&mut self) -> XorModel {
        let mut model = XorModel(Vec::new());

        self.retain(|(id, _)| id != &Identifier::new(0));

        while !(self.is_empty()) {
            self.retain(|(id, clause)| match clause.literals[..] {
                [] => {
                    trace!("{} is ⊥", id);
                    trace!("");

                    model.push(Literal::new(*id, false));
                    false
                },

                [x] if x.id() == Identifier::new(0) => {
                    trace!("{} is {}", id, (if x.is_true() { "⊤" } else { "⊥" }));
                    trace!("");

                    model.push(Literal::new(*id, x.is_true()));
                    false
                },

                _ => true,
            });

            self.iter_mut().for_each(|(id, clause)| {
                trace!("{} is associated to {}", id, clause);
                trace!("");

                clause.simplify();

                let mut append = false;

                clause.literals.retain(|literal| {
                    let Some(literal_model) = model.iter().find(|model_literal| model_literal == &literal) else {
                        return true;
                    };

                    if literal_model.is_true() ^ literal.is_true() {
                        append = !append;
                    }

                    false
                });

                if append {
                    clause.literals.push(Literal::r#true());
                }
            });
        }

        model
    }
}

#[derive(Debug, Deref, DerefMut)]
pub struct XorModel(Vec<Literal>);

impl Model for XorModel {
    fn valuations(&self) -> &[Literal] {
        &self.0
    }
}
