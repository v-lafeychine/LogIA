#![feature(iterator_try_collect)]
#![feature(once_cell)]

#[macro_use]
extern crate pest_derive;

pub mod clause;
pub mod identifier;
pub mod instance;
pub mod parser;
pub mod solver;
