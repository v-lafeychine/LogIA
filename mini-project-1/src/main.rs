use std::path::Path;

use anyhow::Result;
use clap::{Parser, Subcommand};
use mini_project_1::instance::{Horn, Instance, Xor};
use mini_project_1::solver::Model;

#[derive(Parser)]
struct Args {
    #[command(subcommand)]
    command: Command,

    /// Toggle the trace output
    #[arg(short, long)]
    trace: bool,
}

#[derive(Subcommand)]
enum Command {
    /// Solve a Horn instance
    Horn { path: String },

    /// Solve a Xor instance
    Xor { path: String },
}

fn main() -> Result<()> {
    let args = Args::parse();

    if args.trace {
        std::env::set_var("RUST_LOG", "trace");
    }

    env_logger::builder()
        .format_target(false)
        .format_timestamp(Some(env_logger::fmt::TimestampPrecision::Micros))
        .init();

    match args.command {
        Command::Horn { path } => resolve::<Horn>(Path::new(&path)),
        Command::Xor { path } => resolve::<Xor>(Path::new(&path)),
    }
}

fn resolve<I: Instance>(path: &Path) -> Result<()> {
    use mini_project_1::parser::Parser;
    use mini_project_1::solver::Solver;

    let problem = I::Parser::parse_file(path)?;

    match I::Solver::solve(&problem) {
        Some(model) => {
            print!("SAT");
            for valuation in model.valuations() {
                print!(" {}", valuation);
            }
            println!();
        },
        None => println!("UNSAT"),
    };

    Ok(())
}
