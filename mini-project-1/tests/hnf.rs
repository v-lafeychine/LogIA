use mini_project_1::clause::horn::HornClause;
use mini_project_1::parser::horn::HornParser;
use mini_project_1::parser::Parser;
use mini_project_1::solver::horn::{HornModel, HornSolver};
use mini_project_1::solver::{Model, Solver};

fn clause_sat(model: &HornModel, clause: &HornClause) -> bool {
    let mut sat = clause.hypothesis().iter().any(|lit| !model.valuation(lit.id()).unwrap());
    if let Some(conclusion) = clause.conclusion() {
        sat = sat || model.valuation(conclusion.id()).unwrap()
    }
    sat
}

fn problem_sat(model: &HornModel, problem: &[HornClause]) -> bool {
    problem.iter().all(|clause| clause_sat(model, clause))
}

fn test_problem(problem_str: &str, sat: bool) -> bool {
    let problem = HornParser::parse(problem_str).unwrap();
    let result = HornSolver::solve(&problem);
    if let Some(model) = result { problem_sat(&model, &problem) && sat } else { !sat }
}

#[test]
fn sat_1() {
    let test = "p hnf 4 5
    1 0
    2 0
    3 0
    4 0
    -1 -2 -3 4";
    assert!(test_problem(test, true));
}
#[test]
fn sat_2() {
    let test = "p hnf 100 2
    -1 -2 -3 4 0
    -4 0";
    assert!(test_problem(test, true));
}
#[test]
fn sat_3() {
    let test = "p hnf 4 5
    1 0
    -1 2 0
    -2 3 0
    -3 4 0
    4 0";

    assert!(test_problem(test, true));
}

#[test]
fn sat_trivial_1() {
    let test = "p hnf 3 3
    -1 -2 3 0
    -2 -3 1 0
    -1 -3 2 0";

    assert!(test_problem(test, true));
}

#[test]
fn sat_trivial_2() {
    let test = "p hnf 3 3
    1 0
    2 0
    3 0";

    assert!(test_problem(test, true));
}

#[test]
fn unsat_1() {
    let test = "p hnf 4 5
    1 0
    -1 2 0
    -2 3 0
    -3 4 0
    -1 -2 -3 -4 0";
    assert!(test_problem(test, false));
}
#[test]
fn unsat_2() {
    let test = "p hnf 10000 8
    1 0
    -1 2 0
    -2 3 0
    2 0
    -5 -37 100 0
    5 0
    -5 37 0
    -100 0";
    assert!(test_problem(test, false));
}
#[test]
fn unsat_3() {
    let test = "p hnf 3 4
    1 0
    2 0
    3 0
    -1 -2 -3 0";
    assert!(test_problem(test, false));
}

#[test]
fn unsat_trivial_1() {
    let test = "p hnf 1 2
    1 0
    -1 0";
    assert!(test_problem(test, false));
}
#[test]
fn unsat_trivial_2() {
    let test = "p hnf 4 6
    1 0
    2 0
    3 0
    4 0
    -1 -2 -3 -4 0";
    assert!(test_problem(test, false));
}
