use mini_project_1::identifier::Identifier;
use mini_project_1::parser::xor::XorParser;
use mini_project_1::parser::Parser;
use mini_project_1::solver::xor::{XorModel, XorSolver};
use mini_project_1::solver::{Model, Solver};

fn assert_valuation(model: XorModel, value: &[bool]) {
    value.iter().enumerate().for_each(|(i, &v)| {
        assert_eq!(model.valuation(Identifier::new(i + 1)).unwrap(), v);
    });
}

#[test]
fn xor_sat() {
    let input = r#"
        p xnf 4 4
         1     3  4 0
            2 -3  4 0
         1  2    -4 0
         1 -2 -3    0
    "#;

    let problem = XorParser::parse(input).unwrap();

    let Some(model) = XorSolver::solve(&problem) else { panic!("Expected a model"); };

    assert_valuation(model, &[false, true, false, true]);
}

#[test]
fn xor_unsat() {
    let input = r#"
        p xnf 1 2
         1 0
        -1 0
    "#;

    let problem = XorParser::parse(input).unwrap();

    let None = XorSolver::solve(&problem) else { panic!("Expected a failure"); };
}
